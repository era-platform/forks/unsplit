

%% Type defs

-type merge_actions() :: [{write, any()} | {delete, any()}].

-type merge_strategy() :: same | all_keys | {atom(), atom()}.

-type merge_ret() ::  stop
            | {ok, any()}
            | {ok, merge_actions(), any()}
            | {ok, merge_actions(), merge_strategy(), any()}.


-define(LOG(Fmt,Args), do_log(Fmt,Args)).
-define(LOG(Level,Fmt,Args), do_log(Level,Fmt,Args)).
-define(OUT(Fmt,Args), do_out(Fmt,Args)).
-define(OUT(Level,Fmt,Args), do_out(Level,Fmt,Args)).

do_log(Fmt,Args) -> do_log('$warning',Fmt,Args).
do_log(Level,Fmt,Args) ->
    case application:get_env(unsplit,log_function) of
        {ok,F} when is_function(F,2) -> F(Fmt,Args);
        {ok,F} when is_function(F,3) -> F(Level,Fmt,Args);
        _ -> io:fwrite(Fmt++"~n",Args)
    end.

do_out(Fmt,Args) -> do_out('$warning',Fmt,Args).
do_out(Level,Fmt,Args) ->
    case application:get_env(unsplit,out_function) of
        {ok,F} when is_function(F,2) -> F(Fmt,Args);
        {ok,F} when is_function(F,3) -> F(Level,Fmt,Args);
        _ -> io:fwrite(Fmt++"~n",Args)
    end.